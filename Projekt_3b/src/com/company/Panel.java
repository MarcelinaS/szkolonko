package com.company;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
int szerOkna;
int wysOkna;
public int xPodz = 10;
public int yPodz = 10;

    public Panel(int szerOkna, int wysOkna){
        setPreferredSize(new Dimension(szerOkna ,wysOkna ));
        this.szerOkna = szerOkna;
        this.wysOkna = wysOkna;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g.drawLine(0,wysOkna/2,szerOkna,wysOkna/2); //oś OX
        g.drawLine(szerOkna,wysOkna/2,szerOkna-5,(wysOkna/2)-5); //strzałka OX
        g.drawLine(szerOkna,wysOkna/2,szerOkna-5,(wysOkna/2)+5);

        g.drawLine(szerOkna/2,0,szerOkna/2,wysOkna); //oś OY

        g.drawLine(szerOkna/2,0,(szerOkna/2)-5,5); //strzałka OY
        g.drawLine(szerOkna/2,0,(szerOkna/2)+5,5);

        for (int i = 1 ; i<20 ; i++){                   //podziałka X
            g.drawLine(szerOkna/2 + i * xPodz,(wysOkna/2)-3,szerOkna/2 + i * xPodz,(wysOkna/2)+3);
            g.drawLine(szerOkna/2 - i * xPodz,(wysOkna/2)-3,szerOkna/2 - i * xPodz,(wysOkna/2)+3);
        }

        for (int i = 1 ; i<20 ; i++){                  //podziałka Y
            g.drawLine((szerOkna/2)-3,wysOkna/2 + i*yPodz,(szerOkna/2)+3,wysOkna/2 + i*yPodz);
            g.drawLine((szerOkna/2)-3,wysOkna/2 - i*yPodz,(szerOkna/2)+3,wysOkna/2 - i*yPodz);
        }

        for (int i = 0 ; i < szerOkna-20; i++){
                Funkcja funkcja2 = new Funkcja();
                int y1 = (int) funkcja2.wartoscFunkcji(i,2,0);
                int y2 = (int) funkcja2.wartoscFunkcji(i+1,2,0);

                if (y2 < wysOkna/2 -10)
                {
                g.drawLine(szerOkna/2 + i,wysOkna/2 - y1,szerOkna/2 + i + 1, wysOkna/2 - y2);
                g.drawLine(szerOkna/2 - i,wysOkna/2 + y1,szerOkna/2 - i - 1, wysOkna/2 + y2);
                 }


        }

    }


}
