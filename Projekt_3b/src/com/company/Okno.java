package com.company;

import javax.swing.*;
import java.awt.*;

public class Okno extends JFrame {

    public Okno(int szerOkna, int wysOkna){
        super("Tytuł");



        JPanel panel = new Panel(szerOkna,wysOkna);
        add(panel);

        pack(); // automatyczne dopasowanie do romiarów dodanych komponentów oraz zdef. zarządcy rozkładem
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //w momencie zamknięcia tego okna cała aplikacja ma być zamykana
        setVisible(true);
    }
}