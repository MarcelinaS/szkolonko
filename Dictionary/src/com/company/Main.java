package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Podaj słowo po polsku: ");
        Scanner in = new Scanner(System.in);
        String polishWord = in.next();

        Map<String,String> dictionary  = new HashMap<>();
        dictionary.put("tata","father");
        dictionary.put("pies","dog");
        dictionary.put("kot","cat");
        dictionary.put("Areczek","whale");
        dictionary.put("palec","finger");
        dictionary.put("hejka","hi");

        if (dictionary.containsKey(polishWord.toLowerCase())){
            System.out.println("Słowo po ang: "+dictionary.get(polishWord.toLowerCase()));
        }
        else {
            System.out.println("Nie ma takiego słowa w słowniku");
        }
    }

}
