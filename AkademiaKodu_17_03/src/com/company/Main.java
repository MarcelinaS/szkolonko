package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //KOLEKCJE  bardziej elastyczne niz tablice

        List<String> users = new ArrayList<>(); //preferowana składnia
       // można też: ArrayList<String> users2 = new ArrayList<>();
        users.add("Marcelina");
        users.add("Areczek");
        users.add("Tomasz");
        users.add("Aneczka");
        users.add("Tomasz");
        users.add("Zenon");

        System.out.println(users.size());
        users.remove("Tomasz"); //usuwa tylko pierwszego Tomasza
        //pozostałe przesuwa

        for (String user:users) {
            System.out.println(user);
        }


    }
}
