package edu.wsb.drugieSpotkanie;

public class Petle {

    static double matematyka[];

    public static double zwrocSrednia (){
        matematyka = new double[3]; // tablica trzyelementowa
        matematyka[0] = 4;
        matematyka[1] = 4;
        matematyka[2] = 4;
        double wynik =0;
        for (int i=0; i< matematyka.length; i++){
            wynik = wynik + matematyka[i];
        }
        return wynik/matematyka.length;
    }

    public static void wypiszSrednia() {
        System.out.println(zwrocSrednia());
    }

    public static double zwrocSrednia(double[] przedmiot){
        double wynikOstateczny =0;
        //jeśli znamy wielkość tablicy, kolekcji -> for
        for (double wynik : przedmiot){ //przejdz przez wielkosc tablicy przedmiot i wyciagnij z wnetrza tablicy jednostkowe wyniki
             wynikOstateczny += wynik ;
        }
        return wynikOstateczny;
    }

    public static int obliczDlugoscTablicy(float[] przedmiot){
        int dlugoscTablicy = 0;
        //jeśli dodane będą ekstra warunki - np. przerwania -> while
        while (dlugoscTablicy < przedmiot.length){
            dlugoscTablicy++;
        }
        //do-while wykona się chociaż raz, niezależnie od warunku
        do{
            dlugoscTablicy++;
        }while (dlugoscTablicy < przedmiot.length);

        return dlugoscTablicy;
    }
}


