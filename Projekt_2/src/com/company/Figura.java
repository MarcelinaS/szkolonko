package com.company;

public abstract class Figura {
    private int a;
    private int b;

    public abstract double pole (int a, int b);
    public abstract double obwod (int a, int b);
}
