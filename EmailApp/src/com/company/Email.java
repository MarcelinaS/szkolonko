package com.company;

import java.util.Scanner;

public class Email {
    private String firstname;
    private String lastname;
    private String password;
    private int defaultPasswordLength = 7;
    private String department;
    private String email;
    private int maillboxCapacity = 500;
    private String alternateEmail;
    private String companySuffix = "anycompany.com";

    //constructor to receive f irst and last name
    public Email(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;

        // Call a method asking for department - return the department
        this.department = setDepartment();

        // Call a method that return a random password
        this.password = randomPassword(defaultPasswordLength);
        System.out.println("Your password is " + this.password);

        // Combine elements to generate email
        email = firstname.toLowerCase() + "." + lastname.toLowerCase() + "@" + department + "." + companySuffix;
    }

    ;

    //ask for department
    private String setDepartment() {
        System.out.print("Department codes: \n 1 for sales \n 2 for Development" +
                "\n 3 for Accounting ...\n 0 for none \nEnter code: ");
        Scanner in = new Scanner(System.in);
        int depChoice = in.nextInt();  //department Choice
        if (depChoice == 1) {
            return "sales";
        } else if (depChoice == 2) {
            return "dev";
        } else if (depChoice == 3) {
            return "acct";
        } else {
            return "";
        }
    }

    //generate a random password
    private String randomPassword(int length) {
        String passwordSet = "ABCDEFGHIJKLMNOPRSTUWXYZ123456789!@#$%^&*";
        char[] password = new char[length]; //tworzy się tablica PASSWORD zadanej dług.
        for (int i = 0; i < length; i++) {
            int rand = (int) (Math.random() * passwordSet.length()); //Math.random zwraca 0-1
            password[i] = passwordSet.charAt(rand); //losowy element z Set-u
        }
        return new String(password);
    }

    //set the mailbox capacity
    public void setMailboxCapacity(int capacity) {
        this.maillboxCapacity = capacity;
    }

    //set the alternate email
    public void setAlternateEmail(String altemail) {
        this.alternateEmail = altemail;
    }

    //change the password
    public void changePassword(String password) {
        this.password = password;
    }

    public int getMaillboxCapacity() {
        return maillboxCapacity;
    }

    public String getPassword() {
        return password;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public String showInfo() {
        return "DISPLAY NAME: " + firstname + " "
                + lastname + "\nCOMPANY EMAIL " + email + "\nMAILBOX CAPACITY " + maillboxCapacity + ".mb";
    }
}
