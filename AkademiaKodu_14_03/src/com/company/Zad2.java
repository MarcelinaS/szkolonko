package com.company;

import java.util.Scanner;

public class Zad2 {
    //psvm
    public static void main(String[] args) { //główna
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int number = scanner.nextInt();

        if (number%3 == 0){
            System.out.println("Liczba podzielna przez 3");
        }
        else{
            System.out.println("Liczba niepodzielna przez 3");
        }
    }
}