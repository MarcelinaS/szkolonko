package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Witaj w master apce Kalkulator!");
        System.out.println("Jeśli chcesz skończyć, zamiast rownania wpisz słowo EXIT.");
        boolean czyDzialac = true;
        do {
            System.out.println("Podaj swoje równanie: ");
            Scanner in = new Scanner(System.in);
            String userRownanie = in.next();
            if (userRownanie.equals("EXIT")) {
                czyDzialac = false;
                System.out.println("bye bye!");
                //break;
            }
            else{
                Dzialanie dzialanko = new Dzialanie(userRownanie);
                System.out.println("Wynik, to: " + dzialanko.wynik());
            };

        } while (czyDzialac);

    }
}
