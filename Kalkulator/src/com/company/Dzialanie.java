package com.company;

public class Dzialanie {
    private double a;
    private double b;
    private char typDzialania;
    private int gdzieZnak;
    private int gdzieZnakRownosci;
    String rownanie;

    public Dzialanie(String rownanie){
        try {
            if (rownanie.contains("+")) {
                this.gdzieZnak = rownanie.indexOf('+');
                this.typDzialania = '+';
            } else if (rownanie.contains("-")) {
                this.gdzieZnak = rownanie.indexOf('-');
                this.typDzialania = '-';
            } else if (rownanie.contains("*")) {
                this.gdzieZnak = rownanie.indexOf('*');
                this.typDzialania = '*';
            } else if (rownanie.contains(":")) {
                this.gdzieZnak = rownanie.indexOf(':');
                this.typDzialania = '/';
            } else if (rownanie.contains("/")) {
                this.gdzieZnak = rownanie.indexOf('/');
                this.typDzialania = '/';
            }  else {
                this.typDzialania = 'x';
                this.gdzieZnak = -1;
            }
            if (rownanie.contains("=")) {
                this.gdzieZnakRownosci = rownanie.indexOf('=');
            }
            else {
                this.gdzieZnakRownosci = rownanie.length();
            }
            this.a = Integer.parseInt(rownanie.substring(0,this.gdzieZnak));
            this.b = Integer.parseInt(rownanie.substring(this.gdzieZnak+1,gdzieZnakRownosci));
            //System.out.println(this.typDzialania);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    public double wynik(){
        switch (this.typDzialania){
            case '+' : return a+b ;
            case '-' : return a-b ;
            case '*' : return a*b ;
            case '/' : return a/b ;
        }
        return 100;
    }

}
